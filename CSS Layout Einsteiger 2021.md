# Hacker-School Kurs: Webentwicklung / CSS Einsteiger (Template)

![Stimmungsbild: Webseite mit offenen Entwicklerwerkzeugen](https://hack.allmende.io/uploads/upload_2ee220f782b583bb8849a248467d246f.png)

Wie funktioniert die Anordnung der Dinge auf einer Webseite? In diesem Kurs lernen wir moderne Layout-Techniken mit interaktiven Tutorials.

Darauf aufbauend gestalten wir eine digitale Visitenkarte mit HTML & CSS. Dieser Kurs bietet einen freundlichen Umgang mit {Sonderzeichen}; und Programmier-Begriffen - es sind also keine Vorkenntnisse nötig 🙂.

Alles was du brauchst ist ein Computer _(Windows, Mac, Linux)_, Internet und 'nen Browser _(Firefox, Chrome, ...)_. Gerne mit eigenem Profil/Konto, damit der Surf-Verlauf von anderen am Familien-PC nicht ablenkt, wenn du deinen Bildschirm teilst 😉.

## TODO

- [x] Dokument für Steckbriefe anlegen
- [x] Dokument für 5-Minuten-Fakes upload anlegen
- [ ] [Altes Skript](https://gitlab.com/gitbreaker222/css-eintsteiger/blob/master/CSS%20Layout%20Einsteiger.md#material) übertragen
- [ ] Reverse-Classroom Kurse in Blöcke schreiben
- [x] Links zu Profilkarten auswechseln asdf

## Blöcke

```mermaid
graph LR

video1[/"➤ 30 min Video"/] --> htmlBasics[HTML basics]
htmlBasics --> cssDiner[/"➤ CSS-Diner</br>lvl 1 - 10"/]
cssDiner --> cssBasics[CSS basics]

steckbriefe[/"➤ Steckbriefe"/] --> share[teilen</br>/ veröffentlichen]
share --> fake[/"➤ 5 Minuten fake"/]
fake --> devTools[devTools</br>/ screenshots]

codiMd[/"➤ CodiMD"/] --> textEdit1[kollab.</br>text bearbeiten]
codepen[/"➤ codepen<br/>+ github"/] --> textEdit2[Code edit]
textEdit1 --> mdToHtml[/"➤ MD to HTML"/]
textEdit2 --> mdToHtml
mdToHtml --> rendern
rendern --> profileFork[/"➤ Profile Card fork"/]

froggy[/"➤ FlexboxFroggy"/] --> flexbox[flexbox layout]
flexbox --> profileOuter[/"➤ Profile Card</br>Outer layout"/]

garden[/"➤ GridGarden"/] --> grid[grid layout]
grid --> profileInner[/"➤ Profile Card</br>Inner layout"/]
```

# Ziel

Wir gestalten das Layout einer Profilkarte, um von hier ...  
![screenshot profile card template](https://hack.allmende.io/uploads/upload_7a9ee99d391ebcc390caa678869e282f.png)  
https://codepen.io/Breaker222/pen/xxgOJJB  
... nach da ...  
![screenshot profile card goal](https://hack.allmende.io/uploads/upload_fa33d91c979e7a904f7b4e043c196a51.png)  
https://codepen.io/Breaker222/pen/wvgzeaB  
... zu kommen.


# Basics

```mermaid
graph LR

video1[/"➤ 30 min Video"/] --> htmlBasics[HTML basics]
htmlBasics --> cssDiner[/"➤ CSS-Diner</br>lvl 1 - 10"/]
cssDiner --> cssBasics[CSS basics]
```

## HTML


- HTML Tutorial Deutsch Grundlagen [2021] _(30 Min.)_ https://www.youtube.com/watch?v=vPqagMmpO0Y

1. Was bedeutet `<h1>`?
1. Im `<html>` Grundgerüst gibt es zwei Elemente. Welche?
2. Was bedeutet `<div>`?
3. Wie schreibt man ein _Closing Tag_?

(_Weitere Video-Tutorials - schau mal rein_ :eyes:)

- HTML Tutorial - How to Make a Super Simple Website (30 Min.) https://www.youtube.com/watch?v=PlxWf493en4
- HTML Tutorial Deutsch für Anfänger [2021] (60 Min., ausführlich) https://www.youtube.com/watch?v=3Djmh7V70NE

## CSS

Spiel: *CSS diner* auf flukeout.github.io (Level 1 - 10)

- Breakoutrooms in 3er Gruppen _(45 Min)_


# Text schreiben / Code lesen

```mermaid
graph LR

steckbriefe[/"➤ Steckbriefe"/] --> share[teilen</br>/ veröffentlichen]
share --> fake[/"➤ 5 Minuten fake"/]
fake --> devTools[devTools</br>/ screenshots]

```

## Steckbriefe / Beeing in web dev / Personalities

Getting to know terms, role models, places and a feeling for the variety of topics.

- Introducing Profiles *(5 - 10 min)*
  - Una Kravets https://codepen.io/una
  - Sarah Drasner https://codepen.io/sdras
  - Lea Verou https://codepen.io/leaverou
  - Jen Simmons https://codepen.io/jensimmons
  - Rachel Andrew https://www.smashingmagazine.com/author/rachel-andrew
  - Lyza Gardner https://www.lyza.com
  - Sara Vieira https://iamsaravieira.com
- Research *(\~30 min)*
  - Copy this template, paste it [**here**](https://hack.allmende.io/YpL__0G8QBO63NEzOUazyw#) and fill out the `???` gaps
  - 
    ```md
    # Name

    She comes from *???* and works in/at *???*

    Her skills are:
    - ?
    - ?
    - ?

    ## Examples

    She talked at *???* about [*???*](link-to-video)
    ```
- Presentation *(10 - 20 min)*

Example:

```md
# Rich Harris

He comes from *USA/England ??* and works in *the New York Times*.

His skills are:
- Data visualization
- Svelte (Modern Frontend-Framework)
- philosophy (He graduated at University College London)

## Examples

He talked at YGLF (You gotta love frontend) about [rethinking reactivity](https://www.youtube.com/watch?v=AdNJ3fydeao)
```

## 5-Minuten-Fakes!

![screenshot von fake titel](https://hack.allmende.io/uploads/upload_bf33af08ff0f077b1cb984cad30e028b.png)

Wir nehmen irgendwelche Online-Artikel und ändern mit den Browser Dev-Tools Wörter, so dass witzige Meldungen entstehen und machen screenshots davon
 - Klicke z.B. auf deutschlandfunknova.de irgendeinen Artikel
 - Wort in Titel inspizieren: Rechtsklick > _Element untersuchen_
   - Dev-Tools öffnen sich unten/seitlich
   - ...oder per Tastenkürzel: Windows, Linux <kbd>F12</kbd>; Mac <kbd>Cmd</kbd>+<kbd>Shift</kbd>+<kbd>I</kbd>
 - Screenshot machen. z.B.:
   - Firefox: in Adressleiste _"Aktionen für Seite"_
   - Windows: snipping tool
   - Mac: <kbd>Cmd</kbd>+<kbd>Shift</kbd>+<kbd>4</kbd>
 - Screenshot einfügen: https://hack.allmende.io/cNLcFPDYSPu6go-YUL8PJw#
- Review



# Code schieben

```mermaid
graph LR

codiMd[/"➤ CodiMD"/] --> textEdit1[kollab.</br>text bearbeiten]
codepen[/codepen/] --> textEdit2[Code edit]
textEdit1 --> mdToHtml[/"➤ MD to HTML"/]
textEdit2 --> mdToHtml
mdToHtml --> rendern
rendern --> profileFork[/"➤ Profile Card fork"/]
```

TODO steckbriefe als html in codepen

# Code schreiben

## Profile Card: Outer Layout

```mermaid
graph LR

froggy[/"➤ FlexboxFroggy"/] --> flexbox[flexbox layout]
flexbox --> profileOuter[/"➤ Profile Card</br>Outer layout"/]
```

TODO

## Profile Card: Inner Layout

```mermaid
graph LR

garden[/"➤ GridGarden"/] --> grid[grid layout]
grid --> profileInner[/"➤ Profile Card</br>Inner layout"/]
```

TODO

----

# Software, Accounts, Lernmaterial, etc.

## Code-Editor offline

- [Visual Studio Code](https://code.visualstudio.com/) (VS Code): frei und open-source; heutiger Standard; unterstützt von Microsoft
- [VS Codium](https://vscodium.com/) gleiche Codebasis wie VS Code, aber ohne telemetry/tracking und kleinerer Erweiterungsplattform
- [Webstorm](https://www.jetbrains.com/webstorm/) (kommerziell von IntelliJ)

## Code-Editor online anonym

- jsfiddle.net online Codeeditor **ohne** Account; einfach

## Online mit Account

github.com ist die größte git- und OpenSource-Plattform. Zusätzlich ist es auch ein **Identity-Provider** _(so wie google, facebook, twitter)_. Mit einem Github-Konto kann man sich auf vielen nützlichen Seiten einloggen. Github wurde ~2017 von Microsoft gekauft, es ist aber getrennt von einem Microsoft-Konto.

- gitlab.com - Alternative zu github, aber auch selbst aufsetzbar
- codepen.io - Social Coding Playground; Easy
- glitch.com - playful HTML/CSS/JS Playground; Easy
- repl.it - Web-based IDE for generall coding
- codesandbox.io - Web-based IDE for Frontend-Development
- codecadamy.com interactive courses
- freecodecamp.org interactive courses
- developer.mozilla.com THE Web-Documentation
- stackoverflow.com THE coding Ask-and-Answers site

![die Anmeldeseite von stackoverflow mit Github als Login-Option](https://hack.allmende.io/uploads/upload_384ecaebe18a67b3b8b56441f0da6136.png)

## Freie Kurse

- code.org/ Skripten mit Codeblocks
  - z.B. Minecraft, Frozen, Dance Party...
- [freecodecamp.org](https://www.freecodecamp.org/learn/responsive-web-design/basic-html-and-html5/say-hello-to-html-elements) "Basic HTML" interactive course
- http://svelte.dev/tutorial Frontend Framework (Fortgeschritten)

## Bezahlte Kurse für Weiterbildung

Anstatt Büchern empfehle ich ein Abo für einen Monat. Gleich Kündigen nicht vergessen!
- https://www.codecademy.com/learn/learn-html
  - interactive; like freecodecamp with more fine UX
- https://scrimba.com/learn/htmlcss
- https://www.linkedin.com/learning/webtechniken-lernen-2-css-grundlagen/werkzeuge-und-beispiel-site-ein-uberblick
  - professioneler videokurs; kostenlos?
- https://www.pluralsight.com/courses/front-end-web-app-html5-javascript-css
  - professional videocourse

## Webseiten von Grund auf (zum lesen)

- developer.mozilla.org/de/docs/Learn/Getting_started_with_the_web


----

# Eure Fragen und Notizen:

- [ ] wie kann man screenshots machen?
- [ ] was bedeutet ul?
- [ ] 



