


# _Girls_ Hacker School: CSS Layout Einsteiger

![HackerSchool - CSS Layout Kurs.png](https://windtanz.windcloud.de/index.php/apps/files_sharing/publicpreview/xX4CMGnLt3npJ8G?x=1674&y=569&a=true&file=HackerSchool%2520-%2520CSS%2520Layout%2520Kurs.png&scalingup=0)
Wie funktioniert die Anordnung der Dinge auf einer Webseite? In diesem Kurs lernen wir moderne Layout-Techniken mit interaktiven Tutorials.

Darauf aufbauend gestalten wir eine digitale Visitenkarte mit HTML & CSS. Dieser Kurs bietet auch einen Einstieg in den Umgang mit {Sonderzeichen}; und Programmier-Begriffen - es sind also keine Vorkenntnisse nötig :)

Alles was du brauchst ist ein Computer _(Windows, Mac, Linux)_, Internet und 'nen Browser _(Firefox, Chrome /-basiert, Safari)_ Gerne mit eigenem Profil/Konto, damit der Surf-Verlauf von anderen am Familien-PC nicht ablenkt, wenn du deinen Bildschirm teilst ;-).

# Vorbereitung

Bevor du dich einklinkst:

- PC mit eigenem Account / Browser-Konto
 - Wir arbeiten zusammen am Browser und besuchen verschiedene Seiten. Die Surf-Chronik anderer Benutzer in der Autovervollständigung soll uns nicht ablenken
- Webcam / Mikro / Headset testen

# Ablauf

## Tag 1 - Block 1

### Vorstellung

- Name / Alter / Stand Code & Computer / deine Entwicklung und Wege

### Ziele

- Online Visitenkarte gestalten
 - Ziel: https://codepen.io/Breaker222/full/ZEQJYvK
- Zugänge für weitere Plattformen / Kurse / Services
 - Github account (ID provider für Stackoverflow, Codepen, Repl etc.)

### Aufwärmen

- 5 Minuten Fake! Wir nehmen irgendwelche Online-Artikel und ändern mit den Browser Dev-Tools Wörter, so dass witzige Meldungen entstehen und machen screenshots davon
 - z.B. von <https://www.deutschlandfunknova.de/>
 - Dev-Tools öffnen F12 / Strg+Shift+I / Cmd+Shift+I
 - Uploads für screenshots: <https://windtanz.windcloud.de/index.php/s/BFwqHzqiwME4adq>
- Review

## Tag 1 - Block 2

Für die Visitenkarte brauchen wir die CSS Kurse. Wer sich aber auf anderen Wegen freier fühlt, kann auch Abläufe mit bunten Blöcken skripten oder lernen, wie hosting funktioniert oder moderne Webapp-Frameworks funktionieren. Schau mal die Liste unten bei "Tutorials" durch :)

### CSS (Hauptpfad)

- https://flexboxfroggy.com
- https://cssgridgarden.com

## Tag 1 - Block 3

Visitenkarte kopieren
- von https://codepen.io/Breaker222/pen/RwrZRdy
- nach jsfiddle
-----
- Willkommen Wie geht's
- Heute
 - Persönlichkeiten
 - Visitenkarte gestalten
 - Github

## Tag 2 - Block 1

12:50 Uhr - Einloggen in euren Raum und direkt in den Kurs starten
- Frage aktiv nach, wie die Teilnehmenden die Inhalte heute wahrnehmen. Die Hacker School ist bewusst auf zwei Tage ausgelegt, weil “im Schlaf gelernt” wird. Das ist zwar paradox, aber unser Gehirn funktioniert eben so.
- Ausblick auf die Kursinhalte und die Tagesstruktur (u.a. Ankündigung der Vorstellung des Team Projekte zum Kursende)
- Ganz nach Kursinhalt, eine Erklärphase oder direkt in die zweier Teams.
- 
### Being in web dev. Personalities

Getting to know terms, female role models, places and a feeling for the variety of topics.

- Introducing Profiles *(5 - 10 min) (? biased proposals, edits are welcome)*
  - Una Kravets https://codepen.io/una
  - Sarah Drasner https://codepen.io/sdras
  - Lea Verou https://codepen.io/leaverou
  - Jen Simmons https://codepen.io/jensimmons
  - Rachel Andrew https://www.smashingmagazine.com/author/rachel-andrew
  - Lyza Gardner https://www.lyza.com
  - Sara Vieira https://iamsaravieira.com
- Research *(\~30 min)*
  - What are their fields / specializations / skills / job names ?
  - Where do they speak / about what?
  - Create a new REPL and use the collaboration feature, to document results in a file called "infos.md" *(template below)*
  - Prepare for a small presentation *(2 - 5 min)*
- Presentation *(10 - 20 min)*

Template 

```
# Name

She comes from *???* and works in/at *???*

Her skills are:
- ?
- ?
- ?

## Examples

She talked at *???* about [*???*](www.something.com/link-to-talk)
```

Example:

```
# Rich Harris

He comes from *USA/England (??)* and works in/at *the New York Times*.

His skills are:
- Data visualization
- Svelte
- philosophy (He graduated at University College London)

## Examples

He talked at YGLF (You gotta love frontend)  about [rethinking reactivity](https://www.youtube.com/watch?v=AdNJ3fydeao)
```

## Tag 2 - Block 2

Grid Garden und Visitenkarte
- Use your own colour/image selection _(30 - 40 min)_
 -   background
 -   profile image
 -   card background
 -   [www.unsplash.com](http://www.unsplash.com)
 -   Credit the creator of your used image at the end of your description text
 
## Tag 2 - Block 3

- Was ist Github (Einschub)
 - git = Versionierungs-Software
 - Größte Open-Source Plattform
 - ID-Provider (wie facebook oder twitter)
- Visitenkarten anschauen
 - wer mag im großen Raum zeigen (link)?
 - bis dahin weiter hacken
 
# Tutorials

Kurs-Palette: Hier sollte für jede was dabei sein. 

## CSS (Hauptpfad)

- https://flexboxfroggy.com
- https://cssgridgarden.com

## HTML Videotutorials

- in 30 minutes (english) https://www.youtube.com/watch?v=PlxWf493en4
- Full HTML Videotutorial 2018/2019 (english) https://www.youtube.com/watch?v=gQojMIhELvM&list=PLoYCgNOIyGAB_8_iq1cL8MVeun7cB6eNc
## Skripten / Codeblocks

- https://code.org/ Kurse
   - z.B. Minecraft, Frozen, Dance Party...
 
## Frameworks (Fortgeschritten)

- http://svelte.dev/tutorial

## Webseiten von Grund auf (zum lesen)

- https://developer.mozilla.org/de/docs/Learn/Getting_started_with_the_web

## Interaktive Kurse für Weiterbildung

Anstatt Bücher empfehle ich ein Abo für einen Monat. Gleich Kündigen nicht vergessen.
- https://www.codecademy.com/learn/learn-html
- https://scrimba.com/learn/htmlcss
- https://www.linkedin.com/learning/webtechniken-lernen-2-css-grundlagen/werkzeuge-und-beispiel-site-ein-uberblick
- https://www.pluralsight.com/courses/front-end-web-app-html5-javascript-css

# Material

## Dokumentation

- HTML Spickzettel: https://www.codecademy.com/learn/learn-html/modules/learn-html-elements/cheatsheet
- Alles: developer.mozilla.org

## Codeeditor online ohne Account

- https://jsfiddle.net/

## Codeeditor online mit Account (github)

- [Codepen](codepen.io)
- [Codesandbox](codesandbox.io)
- [REPL](repl.it)
- 
## Codeeditor offline

- [Visual Studio Code](https://code.visualstudio.com/) (VS Code): frei und open-source; heutiger Standard; unterstützt von Microsoft
- [VS Codium](https://vscodium.com/) gleiche Codebasis wie VS Code, aber ohne telemetry/tracking und kleinerer Erweiterungsplattform
- [Webstrom / PHPStorm](https://www.jetbrains.com/webstorm/) (kommerziell von IntelliJ)

Es gibt eine Reihe weiter Programme, die heute oft wieder veraltete sind _(älteste zuerst)_:
- Notepad ++
- AptanaStudio
- Sublime Text
- Brackets
- Atom

# Ergebnisse

2019
- [https://jsfiddle.net/x8ayuwmg/](https://jsfiddle.net/x8ayuwmg/)
2021-01-10
- [https://jsfiddle.net/jdu35Lhs/](https://jsfiddle.net/jdu35Lhs/)
- [https://jsfiddle.net/3hrab79g/2/](https://jsfiddle.net/3hrab79g/2/)
- [https://jsfiddle.net/ryahdwj6/1/](https://jsfiddle.net/ryahdwj6/1/)
- [https://jsfiddle.net/5ftjn6pk/7/](https://jsfiddle.net/5ftjn6pk/7/)
- [https://jsfiddle.net/ryahdwj6/1/](https://jsfiddle.net/ryahdwj6/1/)
<!--stackedit_data:
eyJoaXN0b3J5IjpbNzk0MTI0NTE1LC01Mjg2NzAzNjUsNTE5Mj
k3NzE4LDE2MzM3MjExNTUsLTcxOTk2MjA1OCw5MjU1ODg5MjYs
LTI3OTc3OTIzNiwtMTUxMzE0NjMwLDE0MDkxNTE1MzQsLTkwMD
Q0NjcxMCwxOTUzNTI2MTYyLC0xNTU1ODgyNDAzLC01MjAwNDg1
NjQsMTc3MTU3Nzg1NCwxMzg4MDgxNzQwLC0xNzQ1NzE1MzA5LC
0xODU1MzA0NjA5LDE1ODYyMzY5OSw1NzE1Mzc5MDgsMTI0NzI1
ODM4M119
-->