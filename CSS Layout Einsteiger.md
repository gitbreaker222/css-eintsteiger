<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CSS Layout Einsteiger.1</title>
  <link rel="stylesheet" href="https://stackedit.io/style.css" />
</head>

<body class="stackedit">
  <div class="stackedit__left">
    <div class="stackedit__toc">
      
<ul>
<li><a href="#girls-hacker-school-css-layout-einsteiger">Girls Hacker School: CSS Layout Einsteiger</a></li>
<li><a href="#vorbereitung">Vorbereitung</a></li>
<li><a href="#al">l</a>
<ul>
<li><a href="#ta-1"> </a></li>
<li><a href="#tl-">T l</a></li>
<li><a href="#t--block">  lock</a></li>
<li><a href="#aok-">aok </a></li>
<li><a href="#t-----">    </a></li>
<li><a href="#ta---bl">a   l</a></li>
</ul>
</li>
<li><a href="#al">l</a>
<ul>
<li><a href="#taTa</a></li>
<li><a href="#html-videotutorials">HTML Videotutorials</a></li>
<li><a href="#t--block">  lock</a></li>
<li><a href="#a-oc">a oc</a></li>
<li><a href="#tg---l">a  l</a></li>
<li><a href="#ta---bl">a   l</a></li>
</ul>
</li>
<li><a href="#material">Material</a>
<ul>
<li><a href="#dokumentation">Dokumentation</a></li>
<li><a href="#codeeditor-online-account">Codeeditor online oh)</a></li>
<li><a href="#codeeditor-online-mit-account-github">Codeeditor online mit Account (github)</a></li>
<li><a href="#codeeditor-offline">Codeeditor offline</a></li>
</ul>
</li>
<li><a href="#ergebnisse">Ergebnisse</a></li>
</ul>

    </div>
  </div>
  <div class="stackedit__right">
    <div class="stackedit__html">
      <h1 id="girls-hacker-school-css-layout-einsteiger"><em>Girls</em> Hacker School: CSS Layout Einsteiger</h1>
<p><img src="https://windtanz.windcloud.de/index.php/apps/files_sharing/publicpreview/xX4CMGnLt3npJ8G?x=1674&amp;y=569&amp;a=true&amp;file=HackerSchool%2520-%2520CSS%2520Layout%2520Kurs.png&amp;scalingup=0" alt="HackerSchool - CSS Layout Kurs.png"><br>
Wie funktioniert die Anordnung der Dinge auf einer Webseite? In diesem Kurs lernen wir moderne Layout-Techniken mit interaktiven Tutorials.</p>
<p>Darauf aufbauend gestalten wir eine digitale Visitenkarte mit HTML &amp; CSS. Dieser Kurs bietet auch einen Einstieg in den Umgang mit {Sonderzeichen}; und Programmier-Begriffen - es sind also keine Vorkenntnisse nötig :)</p>
<p>Alles was du brauchst ist ein Computer <em>(Windows, Mac, Linux)</em>, Internet und 'nen Browser <em>(Firefox, Chrome /-basiert, Safari) Gerne mit eigenem Profil/Konto, damit der Surf-Verlauf von anderen am Familien-PC nicht ablenkt, wenn du deinen Bildschirm teilst ;-)</em>.</p>
<h1 id="vorbereitung">Vorbereitung</h1>
<ul>
<li>PC mit eigenem Account / Browser-Konto</li>
<li>Wir arbeiten zusammen am Browser und besuchen verschiedene Seiten. Die Surf-Chronik anderer Benutzer in der Autovervollständigung soll uns nicht ablenken</li>
<li>Webcam / Mikro / Headset testen</li>
</ul>
<h1 id="ablauf">Ablauf</h1>
<h2 id="tag-1---block-1">Tag 1 - Block 1</h2>
<h3 id="vorstellung">Vorstellung</h3>
<ul>
<li>Name / Alter / Stand Code &amp; Computer / deine Entwicklung und Berufswege</li>
</ul>
<h3 id="ziele">Ziele</h3>
<ul>
<li>Online Visitenkarte gestalten</li>
<li>Ziel: <a href="https://codepen.io/Breaker222/full/ZEQJYvK">https://codepen.io/Breaker222/full/ZEQJYvK</a></li>
<li>Zugänge für weitere Plattformen / Kurse / Services</li>
<li>Github account (ID provider für Stackoverflow, Codepen, Repl etc.)</li>
</ul>
<h3 id="aufwärmen">Aufwärmen</h3>
<ul>
<li>5 Minuten Fake! Wir nehmen irgendwelche Online-Artikel und ändern mit den Browser Dev-Tools Wörter, so dass witzige Meldungen entstehen und machen screenshots davon</li>
<li>z.B. von <a href="https://www.deutschlandfunknova.de/">https://www.deutschlandfunknova.de/</a></li>
<li>Dev-Tools öffnen F12 / Strg+Shift+I / Cmd+Shift+I</li>
<li>Uploads für screenshots: <a href="https://windtanz.windcloud.de/index.php/s/BFwqHzqiwME4adq">https://windtanz.windcloud.de/index.php/s/BFwqHzqiwME4adq</a></li>
<li>Review</li>
</ul>
<h2 id="tag-1---block-2">Tag 1 - Block 2</h2>
<p>Für die Visitenkarte brauchen wir die CSS Kurse. Wer sich aber auf anderen Wegen freier fühlt, kann auch Abläufe mit bunten Blöcken skripten oder lernen, wie hosting funktioniert oder moderne Webapp-Frameworks funktionieren. Schau mal die Liste en bei “Tutorials” durch :)</p>
<h3 id="css-hauptpfad">CSS (Hauptpfad)</h3>
<ul>
<li><a href="https://flexboxfroggy.com">https://flexboxfroggy.com</a></li>
<li><a href="https://cssgridgarden.com">https://cssgridgarden.com</a></li>
</ul>
<h2 id="tag-1---block-3">Tag 1 - Block 3</h2>
<p>Visitenkarte kopieren</p>
<ul>
<li>von <a href="https://codepen.io/Breaker222/pen/RwrZRdy">https://codepen.io/Breaker222/pen/RwrZRdy</a></li>
<li>nach jsfiddle</li>
</ul>
<hr>
<ul>
<li>Willkommen Wie geht’s</li>
<li>Heute</li>
<li>Persönlichkeiten</li>
<li>Visitenkarte gestalten</li>
<li>Github</li>
</ul>
<h2 id="tag-2---block-1">Tag 2 - Block 1</h2>
<p>12:50 Uhr - Einloggen in euren Raum und direkt in den Kurs starten</p>
<ul>
<li>Frage aktiv nach, wie die Teilnehmenden die Inhalte heute wahrnehmen. Die Hacker School ist bewusst auf zwei Tage ausgelegt, weil “im Schlaf gelernt” wird. Das ist zwar paradox, aber unser Gehirn funktioniert eben so.</li>
<li>Ausblick auf die Kursinhalte und die Tagesstruktur (u.a. Ankündigung der Vorstellung des Team Projekte zum Kursende)</li>
<li>Ganz nach Kursinhalt, eine Erklärphase oder direkt in die zweier Teams.</li>
<li></li>
</ul>
<h3 id="being-in-web-dev.-personalities">Being in web dev. Personalities</h3>
<p>Getting to know terms, female role models, places and a feeling for the variety of topics.</p>
<ul>
<li>Introducing Profiles <em>(5 - 10 min) (? biased proposals, edits are welcome)</em>
<ul>
<li>Una Kravets <a href="https://codepen.io/una">https://codepen.io/una</a></li>
<li>Sarah Drasner <a href="https://codepen.io/sdras">https://codepen.io/sdras</a></li>
<li>Lea Verou <a href="https://codepen.io/leaverou">https://codepen.io/leaverou</a></li>
<li>Jen Simmons <a href="https://codepen.io/jensimmons">https://codepen.io/jensimmons</a></li>
<li>Rachel Andrew <a href="https://www.smashingmagazine.com/author/rachel-andrew">https://www.smashingmagazine.com/author/rachel-andrew</a></li>
<li>Lyza Gardner <a href="https://www.lyza.com">https://www.lyza.com</a></li>
<li>Sara Vieira <a href="https://iamsaravieira.com">https://iamsaravieira.com</a></li>
</ul>
</li>
<li>Research <em>(~30 min)</em>
<ul>
<li>What are their fields / specializations / skills / job names ?</li>
<li>Where do they speak / about what?</li>
<li>Create a new REPL and use the collaboration feature, to document results in a file called “<a href="http://infos.md">infos.md</a>” <em>(template below)</em></li>
<li>Prepare for a small presentation <em>(2 - 5 min)</em></li>
</ul>
</li>
<li>Presentation <em>(10 - 20 min)</em></li>
</ul>
<p>Template</p>
<pre><code># Name

She comes from *???* and works in *???*

Her skills are:
- ?
- ?
- ?

## Examples

She talked at *???* about [*???*](www.something.com/link-to-talk)
</code></pre>
<p>Example:</p>
<pre><code># Rich Harris

He comes from *USA/England ??* and works in *the New York Times*.

His skills are:
- Data visualization
- Svelte
- philosophy (He graduated at University College London)

## Examples

He talked at YGLF (You gotta love frontend)  about [rethinking reactivity](https://www.youtube.com/watch?v=AdNJ3fydeao)
</code></pre>
<h2 id="tag-2---block-2">Tag 2 - Block 2</h2>
<p>Grid Garden und Visitenkarte</p>
<ul>
<li>Use your own colour/image selection <em>(30 - 40 min)</em></li>
<li>background</li>
<li>profile image</li>
<li>card background</li>
<li><a href="http://www.unsplash.com">www.unsplash.com</a></li>
<li>Credit the creator of your used image at the end of your description text</li>
</ul>
<h2 id="tag-2---block-3">Tag 2 - Block 3</h2>
<ul>
<li>Was ist Github (Einschub)</li>
<li>git = Versionierungs-Software</li>
<li>Größte Open-Source Plattform</li>
<li>ID-Provider (wie facebook oder twitter)</li>
<li>Visitenkarten anschauen</li>
<li>wer mag im großen Raum zeigen (link)?</li>
<li>bis dahin weiter hacken</li>
</ul>
<h1 id="tutorials">Tutorials</h1>
<p>Kurs-Palette: Hier sollte für jede was dabei sein.</p>
<h2 id="css-hauptpfad-1">CSS (Hauptpfad)</h2>
<ul>
<li><a href="https://flexboxfroggy.com">https://flexboxfroggy.com</li>
<li>ps://cssgridgarden.com">https://cssgridg
<h2 id="html-videotutorials">HTML Videotutorials</h2>
<ul>
<li>in 30 minutes (english) <a href="https://www.youtube.com/watch?v=PlxWf493en4">https://www.youtube.com/watch?v=PlxWf493en4</a></li>
<li>Full HTML Videotutorial 2018/2019 (english) <a href="https://www.youtube.com/watch?v=gQojMIhELvM&amp;list=PLoYCgNOIyGAB_8_iq1cL8MVeun7cB6eNc">https://www.youtube.com/watch?v=gQojMIhELvM&amp;list=PLoYCgNOIyGAB_8_iq1cL8MVeun7cB6eNc</a><t
en--codeblocks">Skripten / Codeblocks</h2>
<ul>
<li><a href="https://code.org/">https://code.org/</a> Kurse</li>
<li>z.B. Minecraft, Frozen, Dance Party…</li>
</ul>
<h2 id="frameworks-fortgeschritten">Frameworks (Fortgeschritten)</h2>
<ul>
<li><a href="http://svelte.dev/tutorial">http://svelte.dev/tutorial</a></li>
</ul>
<h2 id="webseiten-von-grund-auf-zum-lesen">Webseiten von Grund auf (zum lesen)</h2>
<ul>
<li><a href="https://developer.mozilla.org/de/docs/Learn/Getting_started_with_the_web">https://developer.mozilla.org/de/docs/Learn/Getting_started_with_the_web</a></li>
</ul>
<h2 id="interaktive-kurse-für-weiterbildung">Interaktive Kurse für Weiterbildung</h2>
<p>Anstatt Bücher empfehle ich ein Abo für einen Monat. Gleich Kündigen nicht vergessen.</l>
<li><a href="https://www.codecademy.com/learn/learn-html">https://www.codecademy.com/learn/learn-html</a></li>
<li><a href="https://scrimba.com/learn/htmlcss">https://scrimba.com/learn/htmlcss</a></li>
<li><a href="https://www.linkedin.com/learning/webtechniken-lernen-2-css-grundlagen/werkzeuge-und-beispiel-site-ein-uberblick">https://www.linkedin.com/learning/webtechniken-lernen-2-css-grundlagen/werkzeuge-und-beispiel-site-ein-uberblick</a></li>
<li><a href="https://www.pluralsight.com/courses/front-end-web-app-html5-javascript-css">https://www.pluralsight.com/courses/front-end-web-app-html5-javascript-css</a></li>
</ul>
<h1 id="material">Material</h1>
<h2 id="dokumentation">Dokumentation</h2>
<ul>
<li>HTML Spickzettel: <a href="https://www.codecademy.com/learn/learn-html/modules/learn-html-elements/cheatsheet">https://www.codecademy.com/learn/learn-html/modules/learn-html-elements/cheatsheet</a></li>
<li>Alles: <a href="http://developer.mozilla.org">developer.mozilla.org</a></li>
</ul>
<h2 id="codeeditor-online-ohne-account">Codeeditor online ohne Account</h2>
<ul>
<li><a href="https://jsfiddle.net/">https://jsfiddle.net/</a></li>
</ul>
<h2 id="codeeditor-online-mit-account-">Codeeditor online mit Account (github)</h2>
<ul>
<li><a href="codepen.io">C</li>
<li><a href="codesandbox.io">Codesandbox</a></li>
<li><a href="repl.it">REPL</a></li>
<li></li>
</ul>
<h2 id="codeeditor-offline">Codeeditor offline</h2>
<ul>
<li>Visual Studio Code (free and open-source)</li>
<li>Webstrom / PHPStorm (von IntelliJ)<br>
Es gibt eine Reihe weiter Programme, die empfohlen werden, aber oft sind auch veraltete dabei:</li>
<li>Notepad ++</li>
<li>AptanaStudio</li>
<li>Sublime Text</li>
<li>Brackets</li>
<li>Atom</li>
</ul>
<h1 id="ergebnisse">Ergebnisse</h1>
<p>2019</p>
<ul>
<li><a href="https://jsfiddle.net/x8ayuwmg/">https://jsfiddle.net/x8ayuwmg/</a><br>
2021-01-10</li>
<li><a href="https://jsfiddle.net/jdu35Lhs/">https://jsfiddle.net/jdu35Lhs/</a></li>
<li><a href="https://jsfiddle.net/3hrab79g/2/">https://jsfiddle.net/3hrab79g/2/</a></li>
<li><a href="https://jsfiddle.net/ryahdwj6/1/">https://jsfiddle.net/ryahdwj6/1/</a></li>
<li><a href="https://jsfiddle.net/5ftjn6pk/7/">https://jsfiddle.net/5ftjn6pk/7/</a></li>
<li><a href="https://jsfiddle.net/ryahdwj6/1/">https://jsfiddle.net/ryahdwj6/1/</a></li>
</ul>

    </div>
  </div>
</body>

</html>
<!--stackedit_data:
eyJoaXN0b3J5IjpbMzg0OTk4MzEzXX0=
-->